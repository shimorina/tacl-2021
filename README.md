This is the repository for the paper:

Anastasia Shimorina, Yannick Parmentier, Claire Gardent. _An Error Analysis Framework for Shallow Surface Realization_. Transactions of the Association for Computational Linguistics (2021) 9: 429–446. [paper](https://doi.org/10.1162/tacl_a_00376)


## Requirements

Tested with Python 3.6.9 on Ubuntu 18.04.5.

Running the code using a virtual environment is highly recommended. To install dependencies, please clone the repository and run:

```bash
cd tacl-experiments/

pip install -r requirements.txt
```

## Code

`tacl-experiments` provides code to reproduce our findings reported in the TACL paper.

`system_output_reader.py`: download models provided by standfordnlp and parse system outputs. Run first with the `----download_models` option to download stanfordnlp models for parsing.

`strip_punct.py`: remove punctuation edges from references (gold parsed trees) and parsed system outputs. (`*-misc-right-ids.conllu` are test files with removed ids from UD originals to match the shared task data.)

`human_eval_file_reading.py`: read original human evaluation files from SR'18 and SR'18. Write evals to .pkl files (fluency and adequacy (raw and z-score)).

`conll_gold_corpus.py`: calculate tree-based metrics on sentence- and corpus-level for treebanks.

`data_stats.py`: Calculate statistics for UD test sets used in SR tasks:
corpus name, instance count, tree depth, tree length, MDD, macro-average mean flux size, mean_flux_weight, mean_arity, % of projective sent.

Soon we will also release our code as a toolkit.

## Data

Please see the websites for [SR'18](http://www.taln.upf.edu/pages/msr2018-ws/SRST.html#data) and [SR'19](http://taln.upf.edu/pages/msr2019-ws/SRST.html#data) shared tasks to get test data, system outputs, and human evaluation data.

Gold trees (=UD treebanks) can be downloaded from the [UD project website](https://universaldependencies.org/). SR'18 used UD v2.0 (some instances were removed in the shared task), and SR'19 used UD v2.3.

To rerun our experiments, please download the data and adjust paths in `config.py`.

