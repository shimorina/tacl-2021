import pandas as pd
from config import *


"""
Read original human evaluation files from SR'18 and SR'18.
Write evals to .pkl files (fluency and adequacy (raw and z-score)).
"""


def read_human_eval(file_adequacy, file_fluency, lang, year):
    adequacy = pd.read_csv(file_adequacy, delim_whitespace=True)
    fluency = pd.read_csv(file_fluency, delim_whitespace=True)
    adequacy.columns = ['SYS', 'sent_id', 'adequacy_raw', 'adequacy_z', 'N_adequacy']
    fluency.columns = ['SYS', 'sent_id', 'fluency_raw', 'fluency_z', 'N_fluency']
    # print(adequacy.head())
    # print(adequacy.shape)
    # print(fluency.head())
    # print(fluency.shape)
    # https://pandas.pydata.org/pandas-docs/stable/user_guide/merging.html#brief-primer-on-merge-methods-relational-algebra
    result = pd.merge(adequacy, fluency, on=['SYS', 'sent_id'], how='outer', validate="one_to_one")
    # print(result.head())
    # print(result.shape)
    result.to_pickle(f'human_eval/{year}-{lang}_all_teams_all_judg_segment_ad_fl.pkl')
    teams = result.groupby(['SYS'])
    for (team_name, data) in teams:
        if year == 'sr19':
            if team_name == 'ADAPT-T1':
                team_name = 'ADAPT_Centre-T1'
            elif team_name == 'RandKart-T1':
                team_name = 'OSU-FB-T1'
        if year == 'sr18':
            if team_name == 'Torino':
                team_name = 'DipInfo'
            elif team_name == 'SaoPaulo':
                team_name = 'NILC'
            elif team_name == 'TUDA':
                team_name = 'BinLin'
        print(data.drop('SYS', axis=1).sort_values(by=['sent_id']).head())
        data.drop('SYS', axis=1).sort_values(by=['sent_id']).to_pickle(f'human_eval/{year}-{team_name}-{lang}.pkl')
        print(f'data written for {lang} {team_name}')


def run_all():
    # sr19
    lang_map = {'en-en': 'en_ewt', 'zh-zh': 'zh_gsd', 'ru-ru': 'ru_syntagrus', 'ud-es': 'es_ancora'}
    for lang in ['en-en', 'zh-zh', 'ru-ru', 'ud-es']:
        read_human_eval(f'{HUMEVAL_SR19}/ad-seg-scores-{lang}.csv',
                        f'{HUMEVAL_SR19}/fl-seg-scores-{lang}.csv',
                        lang_map[lang], 'sr19')

    # sr18
    lang_map = {'en-en': 'en', 'es-es': 'es', 'fr-fr': 'fr'}
    for lang in ['en-en', 'fr-fr', 'es-es']:
        read_human_eval(f'{HUMEVAL_SR18}/ad-seg-scores-{lang}.csv',
                        f'{HUMEVAL_SR18}/fl-seg-scores-{lang}.csv',
                        lang_map[lang], 'sr18')


if __name__ == "__main__":
    run_all()
