import os
from config import *
from utils import read_conllu
from conllu import parse


"""
Remove punct relations from trees. 
Input: conll file and list of deprels to remove, e.g. punct
Remove all edges with punct; while removing, check that it doesn't have children. If so, attach them to parent node.
Output: new conll file with no punct.
"""


def remove_punct_deprel(node):
    """
    Delete nodes with deprel relations recursively.
    First element has always deprel root, no need to remove it.
    Keep all children that are not punct and rewrite them to a parent node.
    :param node: a tree node
    :return: a tree with punct deprel removed
    """
    if node.children:
        children_no_punct = []
        for child in node.children:
            if child.token['deprel'] != 'punct':
                children_no_punct.append(child)
            remove_punct_deprel(child)
        # overwrite children
        node.children = children_no_punct
    return node


def modify_conll(filename, fileout):
    """ Remove punct relations and write a new conll file with recalculated token ids."""
    sentences = read_conllu(filename)
    sents_no_punct = []
    for sentence in sentences:
        sent_no_punct = remove_punct_deprel(sentence)
        tokenlist = parse(sent_no_punct.serialize())  # returns a list of sentences
        # create a mapping 'old_id: new_id'
        id_mapping = {0: 0}  # root remains 0
        for i, item in enumerate(tokenlist[0]):
            id_mapping[item['id']] = i + 1
        # modify ids; if order hasn't been changed, do nothing
        changed = [True if k != v else False for k, v in id_mapping.items()]
        if True in changed:
            # replace ids
            for item in tokenlist[0]:
                item['id'] = id_mapping[item['id']]
                item['head'] = id_mapping[item['head']]
        sents_no_punct.append(tokenlist[0].serialize())
    with open(fileout, 'w+') as f:
        f.write(''.join(sents_no_punct))
    print('Sents written', len(sents_no_punct))
    # print(fileout.split('/')[-1].split('-test')[0])


def remove_punct_sr18():
    # sr18, references
    for lang in LANGS_FULL_SR18:
        modify_conll(UD_SR18_TEST_SET +
                     lang + '-misc-right-ids.conllu',
                     'references/sr18/' + lang + '-test-no-punct.conllu')
    # sr18, systems
    for team in TEAMS_SR18:
        for lang in LANGS_SR18:
            filename = 'preprocessed_outputs/' + team + '_' + lang + '_out.conllu'
            if os.path.isfile(filename):
                modify_conll(filename, 'preprocessed_outputs/sr18/' + team + '_' + lang + '_out_no_punct.conllu')


def remove_punct_sr19():
    # sr19 references
    modify_conll(UD_SR19 + '/UD_Arabic-PADT/ar_padt-ud-test.conllu', 'references/sr19/ar_padt-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_Chinese-GSD/zh_gsd-ud-test.conllu', 'references/sr19/zh_gsd-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_English-EWT/en_ewt-ud-test.conllu', 'references/sr19/en_ewt-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_English-GUM/en_gum-ud-test.conllu', 'references/sr19/en_gum-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_English-LinES/en_lines-ud-test.conllu', 'references/sr19/en_lines-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_English-ParTUT/en_partut-ud-test.conllu', 'references/sr19/en_partut-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_French-GSD/fr_gsd-ud-test.conllu', 'references/sr19/fr_gsd-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_French-ParTUT/fr_partut-ud-test.conllu', 'references/sr19/fr_partut-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_French-Sequoia/fr_sequoia-ud-test.conllu', 'references/sr19/fr_sequoia-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_Hindi-HDTB/hi_hdtb-ud-test.conllu', 'references/sr19/hi_hdtb-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_Indonesian-GSD/id_gsd-ud-test.conllu', 'references/sr19/id_gsd-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_Japanese-GSD/ja_gsd-ud-test.conllu', 'references/sr19/ja_gsd-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_Korean-GSD/ko_gsd-ud-test.conllu', 'references/sr19/ko_gsd-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_Korean-Kaist/ko_kaist-ud-test.conllu', 'references/sr19/ko_kaist-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_Portuguese-Bosque/pt_bosque-ud-test.conllu', 'references/sr19/pt_bosque-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_Portuguese-GSD/pt_gsd-ud-test.conllu', 'references/sr19/pt_gsd-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_Russian-GSD/ru_gsd-ud-test.conllu', 'references/sr19/ru_gsd-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_Russian-SynTagRus/ru_syntagrus-ud-test.conllu', 'references/sr19/ru_syntagrus-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_Spanish-AnCora/es_ancora-ud-test.conllu', 'references/sr19/es_ancora-test-no-punct.conllu')
    modify_conll(UD_SR19 + '/UD_Spanish-GSD/es_gsd-ud-test.conllu', 'references/sr19/es_gsd-test-no-punct.conllu')

    # sr19 systems
    for team in TEAMS_SR19:
        for lang in LANGS_SR19:
            filename = 'preprocessed_outputs/sr19/' + team + '_' + lang + '_out.conllu'
            if os.path.isfile(filename):
                print(lang)
                modify_conll(filename, 'preprocessed_outputs/sr19/' + team + '_' + lang + '_out_no_punct.conllu')


if __name__ == '__main__':
    # remove punct from all UD references: sr19 and sr18
    # remove punct from all systems in all langs: sr19 and sr18
    remove_punct_sr18()
    remove_punct_sr19()
