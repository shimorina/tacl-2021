import pandas as pd
from config import *


"""
Calculate statistics for UD test sets used in SR tasks:
corpus name, instance count, tree depth, tree length, MDD, macro-average mean flux size, mean_flux_weight,
mean_arity, % of projective sent.
"""


def collect_one_line(task, lang,):
    ref_path = f'results/{task}/ref-{lang}.pkl'
    df_lang = pd.read_pickle(ref_path)
    # print(df_lang.describe().round(2))
    one_line = [int(df_lang['sent_id'].count())]
    for column in df_lang.columns[2:]:  # skip sent_id and text
        # cell = f'{df_lang[column].mean().round(2)}+-{df_lang[column].std().round(2)}; [{df_lang[column].min()}; {df_lang[column].max()}]'
        cell = f'{df_lang[column].mean().round(2)}+-{df_lang[column].std().round(2)}'
        if column == 'mean_dep_dist':
            max_value = round(df_lang[column].max(), 2)
            # cell = f'{df_lang[column].mean().round(2)}$\pm${df_lang[column].std().round(2)}; [{df_lang[column].min()}; {max_value}]'
            cell = f'{df_lang[column].mean().round(2)}+-{df_lang[column].std().round(2)}'
        if column == 'projective':
            values_in_percentage = df_lang[column].value_counts(normalize=True) * 100
            projectivity = values_in_percentage.to_dict()
            try:
                cell = round(projectivity[False], 2)
            except KeyError:
                if projectivity[True] == 100.0:
                    cell = 0
                else:
                    print('problem', cell)
        one_line.append(cell)
    final_columns = ['count'] + list(df_lang.columns[2:])
    return one_line,  final_columns


def calculate_stats_per_corpus():
    # sr18, references
    pd.set_option('display.max_columns', None)
    data = []
    for lang in LANGS_SR18:
        one_line, _ = collect_one_line('sr18', lang)
        data.append(one_line)
    # sr19, references
    for lang in LANGS_SR19:
        one_line, final_columns = collect_one_line('sr19', lang)
        data.append(one_line)
    df = pd.DataFrame(data, columns=final_columns, dtype=float)
    df.index = LANGS_SR18 + LANGS_SR19
    cols = ['count', 'tree_depth', 'tree_length', 'mean_dep_dist', 'mean_flux_size', 'mean_flux_weight',
            'mean_arity', 'projective']
    df = df[cols]
    print(df.to_latex())


if __name__ == "__main__":
    calculate_stats_per_corpus()
