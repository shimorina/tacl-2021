LANGS_SR18 = ['ar', 'cs', 'en', 'es', 'fi', 'fr', 'it', 'nl', 'pt', 'ru']
LANGS_FULL_SR18 = ['ar', 'cs', 'en', 'es_ancora', 'fi', 'fr', 'it', 'nl', 'pt', 'ru_syntagrus']
LANGS_SR19 = ['ar_padt', 'en_ewt', 'en_gum', 'en_gum', 'en_lines', 'en_partut', 'es_ancora', 'es_gsd',
              'fr_gsd', 'fr_partut', 'fr_sequoia', 'hi_hdtb', 'id_gsd', 'ja_gsd', 'ko_gsd', 'ko_kaist',
              'pt_bosque', 'pt_gsd', 'ru_gsd', 'ru_syntagrus', 'zh_gsd']


TEAMS_SR18 = ['ADAPT', 'AX', 'BinLin', 'DipInfo', 'IIT-BHU', 'NILC', 'OSU', 'Tilburg']
TEAMS_SR19 = ['ADAPT_Centre', 'BME-UW', 'CLaC', 'CMU', 'DepDist', 'DipInfoUniTo', 'IMS', 'LORIA',
              'OSU-FB', 'RALI', 'Surfers', 'Tilburg']


# paths where to find system outputs for SR'18 and SR'19
SUBMISSIONS_SR18 = '/home/anastasia/Loria/thesis/sr_18/SR18_submissions/'
SUBMISSIONS_SR19 = '/home/anastasia/Loria/thesis/sr_19/SR19_submissions/'

# UD treebanks
UD_SR18_TEST_SET = '/home/anastasia/Loria/thesis/sr_18/ud-test-v2.0-conll2017/gold/conll17-ud-test-2017-05-09/'
UD_SR19 = '/home/anastasia/Loria/thesis/sr_19/ud-treebanks-v2.3'

# SR human evaluation
HUMEVAL_SR18 = '/home/anastasia/Loria/thesis/sr_18/mlsr18-humaneval/analysis'
HUMEVAL_SR19 = '/home/anastasia/Loria/thesis/sr_19/mlsr19-humaneval/analysis'
