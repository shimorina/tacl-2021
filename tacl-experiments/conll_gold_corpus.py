from utils import read_conllu
from utils import read_conllu_linear
from collections import defaultdict
import math


"""
Calculate tree-based metrics on sentence- and corpus-level for UD treebanks.
"""


class Sentence:

    def __init__(self, text, tokens, lemmas, tree, rows):
        self.text = text
        self.tokens = tokens
        self.lemmas = lemmas
        self.tree = tree
        self.rows = rows  # rows of conll

    def dependencies(self):
        # take all edges, construct tuples (child, parent, deprel, distance)
        deps = []
        for row in self.rows:
            if row['deprel'].lower() != 'root':
                parent = self.rows[row['head'] - 1]
                distance = row['id'] - parent['id']
                deps.append((row['lemma'].lower(), parent['lemma'].lower(), row['deprel'], distance))
        return deps

    def dep_distances(self):
        # dependency distances
        distances = [item[3] for item in self.dependencies()]
        abs_distances = [abs(item[3]) for item in self.dependencies()]
        return distances, abs_distances

    def dep_edges(self):
        """
        Take all edges, construct triples <parent_id, child_id, deprel>
        """
        edges = []
        for row in self.rows:
            if row['deprel'].lower() != 'root':
                # parent, child, deprel
                edges.append((row['head'], row['id'], row['deprel']))
        return edges

    def _tree_depth_recursive(self, node):
        max_depth = 0
        if node.children:
            for child in node.children:
                depth_child = self._tree_depth_recursive(child)
                if depth_child > max_depth:
                    max_depth = depth_child
            max_depth += 1
        return max_depth

    def tree_depth(self):
        return self._tree_depth_recursive(self.tree)

    def tree_length(self):
        """How many nodes there are in the tree"""
        return len(self.rows)

    def _is_projective_recursive(self, node, lower_bound, upper_bound):
        """
        Each node should be in the interval [lower_bound, upper_bound]. The upper bound is the parent index;
        the lower bound is the child index. Interval is [0, len(nodes)+1] at initialisation
        For each node, we sort its children by ascending order, and check if children with indices < parent_index are
        on the left and if children with indices > parent_index are on the right.
        """
        result = True
        current_index = node.token['id']
        if node.children:
            # sort children by their indices
            children = [(child, child.token['id']) for child in node.children]
            children.sort(key=lambda x: x[1])
            for child, child_index in children:
                # print(child, child_index, current_index)
                if child_index < current_index:
                    if child_index < lower_bound or child_index > upper_bound:
                        return False
                    result = self._is_projective_recursive(child, lower_bound, current_index)
                    if not result:
                        return False
                else:
                    if child_index < lower_bound or child_index > upper_bound:
                        return False
                    result = self._is_projective_recursive(child, current_index, upper_bound)
                    if not result:
                        return False
        return result

    def is_projective(self):
        return self._is_projective_recursive(self.tree, 0, len(self.rows) + 1)

    def flux(self):
        """
        Return a set of dependencies for each inter-word position (aka flux).
        {1: [dep1, dep2], 2: [dep1, dep3, dep4]} where dep is a triple (parent_id, child_id, deprel)
        """
        flux = {}
        edges = self.dep_edges()
        # no need for info about parent and child, so treat edges as graph edges
        # and sort parent, child in ascending order
        edges = [(edge[0], edge[1]) if edge[0] < edge[1] else (edge[1], edge[0]) for edge in edges]
        for position in range(1, len(self.rows)):
            covered_edges = []
            for edge in edges:
                # collect edges whose beginning index is less than the position index,
                # and whose end index is more than the position index
                if edge[0] <= position < edge[1]:
                    covered_edges.append(edge)
            flux[position] = covered_edges
        return flux

    def mean_flux_size(self):
        """
        Get flux size for each inter-word position and calculate the mean for the given sentence.
        """
        if self.flux():
            flux_sizes = [len(dep_set) for dep_set in self.flux().values()]
            return round(sum(flux_sizes)/len(flux_sizes), 2)
        else:
            return 0

    def _disjoint_deps_in_flux(self, flux, disjoint_deps):
        """Find the biggest subset of disjoint dependencies recursively. See p. 75 of Kahane et al, Depling 2017"""
        # find a vertex that is not shared with other deps
        found = True
        dep_with_no_shared_vertex = ''
        for index, edge in enumerate(flux):
            for dep in flux[index + 1:]:
                if edge[0] == dep[0] or edge[0] == dep[1]:
                    found = False
                    break
            if found:
                dep_with_no_shared_vertex = edge
                other_vertex = edge[1]
                break

            for dep in flux[index + 1:]:
                if edge[1] == dep[0] or edge[1] == dep[1]:
                    found = False
                    break
            if found:
                dep_with_no_shared_vertex = edge
                other_vertex = edge[0]
                break
        if not dep_with_no_shared_vertex:  # it was not possible to find two disjoint dependencies
            disjoint_deps.append((0, 0))  # flux weight is 1
        else:
            disjoint_deps.append(dep_with_no_shared_vertex)
            remaining_flux = []  # delete all the deps that share a vertex with Dep_with_no_shared_vertex
            for edge in flux:
                if edge[0] != other_vertex and edge[1] != other_vertex:
                    remaining_flux.append(edge)
            if remaining_flux:
                disjoint_deps = self._disjoint_deps_in_flux(remaining_flux, disjoint_deps)

        return disjoint_deps

    def flux_weight(self, flux):
        return len(self._disjoint_deps_in_flux(flux, []))

    def mean_flux_weight(self):
        if self.flux():
            flux_weights = [self.flux_weight(dep_set) for dep_set in self.flux().values()]
            # print(flux_weights)
            return round(sum(flux_weights) / len(flux_weights), 2)
        else:
            return 0

    def arity(self, node, arities):
        if node.children:
            arities.append(len(node.children))
            for child in node.children:
                arities = self.arity(child, arities)
        else:
            arities.append(0)
        return arities

    def mean_arity(self):
        """Calculate mean number of dependents of a node"""
        arities = self.arity(self.tree, [])
        # print(arities)
        if arities:
            return round(sum(arities) / len(arities), 2)
        else:
            return 0

    def dependency_overlap(self, lemmas, dependencies):
        """Find tuples of (child, parent) in prediction given gold tuple (child, parent, distance)
        :return: proportion of found dependencies
        """
        count = 0
        for dep in dependencies:
            lemma_gold, parent, _, distance = dep
            indices = [i for i, item in enumerate(lemmas) if item.lower() == lemma_gold.lower()]
            if indices:
                for index in indices:
                    parent_index = index - distance  # child_index - distance = parent_index
                    if len(lemmas) > parent_index >= 0:
                        if lemmas[parent_index].lower() == parent.lower():
                            count += 1
                            # (deprel, 'found', distance)
        return count / len(dependencies) if dependencies else 0

    def dependency_overlap_detailed(self, lemmas, dependencies):
        """Find tuples of (child, parent) in prediction given gold tuple (child, parent, distance)
        and make up a list of (deprel, found/not found, distance)
        :return: a list of stats for each dependency
        """
        summary = []  # [deprel, True/False, distance]
        for dep in dependencies:
            lemma_gold, parent, deprel, distance = dep
            if ':' in deprel:
                deprel = deprel.split(':')[0]  # don't take specific types
            indices = [i for i, item in enumerate(lemmas) if item.lower() == lemma_gold.lower()]
            found = False
            if indices:
                for index in indices:
                    parent_index = index - distance  # child_index - distance = parent_index
                    if len(lemmas) > parent_index >= 0:
                        if lemmas[parent_index].lower() == parent.lower():
                            found = True
                            summary.append([deprel, found, distance])
            if not found:
                summary.append([deprel, found, distance])
        return summary


class Corpus:

    def __init__(self):
        self.sentences = []

    def fill_corpus(self, filename):
        sentences_trees = read_conllu(filename)
        sentences_rows = read_conllu_linear(filename)
        for sent_tree, sent_row in zip(sentences_trees, sentences_rows):
            text = sent_tree.metadata['text']
            tokens = [token['form'] for token in sent_row]
            lemmas = [token['lemma'] for token in sent_row]
            sentence = Sentence(text, tokens, lemmas, sent_tree, sent_row)
            self.sentences.append(sentence)

    def length(self):
        return len(self.sentences)

    def mean_dep_distance(self):
        micro_avg = []
        macro_avg = []
        for sent in self.sentences:
            _, depdists = sent.dep_distances()
            # depdists = [abs(item) for item in sent.dep_distances()]  # convert to absolute values
            if depdists:
                average = sum(depdists) / len(depdists)
                macro_avg.append(average)
                micro_avg += depdists
        print('macro-avg mdd', sum(macro_avg)/len(macro_avg))
        print('micro-avg mdd', sum(micro_avg)/len(micro_avg))

    def deprel_head_directionality(self):
        """
        take deprel without specific types
        :return:
        """
        deprels = defaultdict(dict)  # {deprel: {HI: count, HF: count}} children are to the left or to the right
        for sent in self.sentences:
            deps = sent.dependencies()
            if deps:  # (child, parent, deprel, distance)
                for dep in deps:
                    _, _, deprel, distance = dep
                    if ':' in deprel:
                        deprel = deprel.split(':')[0]
                    head_initial = 1 if distance > 0 else 0
                    head_final = 1 if distance < 0 else 0
                    deprels[deprel]['HI'] = deprels[deprel].get('HI', 0) + head_initial
                    deprels[deprel]['HF'] = deprels[deprel].get('HF', 0) + head_final
        return deprels

    def directionality_entropy(self):
        deprels = self.deprel_head_directionality()
        entropies = {}
        for deprel, counts in deprels.items():
            # word order entropy
            prob_hi = (counts['HI']/(counts['HI'] + counts['HF']))
            prob_hf = (counts['HF']/(counts['HI'] + counts['HF']))
            if prob_hf != 0 and prob_hi != 0:
                entropy = round((- prob_hi * math.log(prob_hi, 2) - prob_hf * math.log(prob_hf, 2)), 2)
            else:
                entropy = 0
            entropies[deprel] = entropy
        return entropies
