import os
import stanfordnlp
import argparse
from config import *


"""
Download models from stanfordnlp and parse submissions for SR'18 and SR'19. Save new parsed files to conllu.
"""


def download_models():
    # Download models only once
    # no models for 'en_partut', 'fr_partut', 'fr_sequoia', 'pt_gsd', 'ru_gsd', 'es_gsd' 'pt_bosque'
    stanfordnlp.download('en')  # en_ewt
    stanfordnlp.download('en_lines')  # en_lines
    stanfordnlp.download('en_gum')  # en_gum -- ??
    stanfordnlp.download('ru')  # ru_syntagrus
    stanfordnlp.download('es')  # es_ancora
    stanfordnlp.download('fr')  # fr_gsd
    stanfordnlp.download('fr_sequoia')  # fr_sequoia -- bad zip file
    stanfordnlp.download('it')  # it_isdt
    stanfordnlp.download('fi')  # fi_tdt
    stanfordnlp.download('ar')  # ar_padt
    stanfordnlp.download('cs')  # cs_pdt
    stanfordnlp.download('nl')  # nl_alpino
    stanfordnlp.download('pt')  # pt_bosque
    stanfordnlp.download('zh')  # zh_gsd
    stanfordnlp.download('hi')  # hi_hdtb
    stanfordnlp.download('id')  # id_gsd
    stanfordnlp.download('ja')  # ja_gsd
    stanfordnlp.download('ko_gsd')  # ko_gsd
    stanfordnlp.download('ko')  # ko_kaist


def output_templates_to_list(nlp, nlp_tokenise, filepath):
    """
    Read predictions, parse each sentence with StanfordNLP Parser
    :param: return: list of parsed sentences in conllu format
    """
    with open(filepath, 'r') as f:
        sents = [line.split('# text =')[1].strip() for line in f if line.startswith('# text =')]

    if not sents:  # try another template
        with open(filepath, 'r') as f:
            sents = [line.split('#text =')[1].strip() for line in f if line.startswith('#text =')]
    if not sents:
        print('Warning! Predictions are in the wrong format.')
    parsed_sentences = []
    bad_sentences = 0  # count how many sentences were parsed into more than one by the parser
    for sentid, sent in enumerate(sents):
        if sent:
            # need to tokenise first and split contractions, and then feed to the parser
            # we do it in order to prevent segmenting into several sentences
            doc_tokenised = nlp_tokenise(sent)
            words = [word.text for sent in doc_tokenised.sentences for word in sent.words]
            doc = nlp(' '.join(words))
            parsed_sentences.append(f'# sent_id = {sentid + 1}\n# text = {sent}\n' + doc.conll_file.conll_as_string())
            '''lemmatised_sent = [word.lemma if word.lemma else '_' for sent in doc.sentences for word in sent.words]
            if None in lemmatised_sent:
                print(lemmatised_sent)
            parsed_sentences.append(' '.join(lemmatised_sent))'''
            if len(doc.sentences) > 1:
                bad_sentences += 1
        else:
            parsed_sentences.append(f'# sent_id = {sentid + 1}\n# text = null_null_null\n1\t_\t_\t_\t_\t_\t_\t_\t_\t_\n\n')
            print('empty prediction sentence was written as null_null_null and 1\t_\t_\t_\t_\t_\t0\troot\t_\t_', sent)

    print('Number of badly parsed sentences detected (parsed to >1 sent):', bad_sentences)
    return parsed_sentences


def parse_sr18():
    path = 'preprocessed_outputs/sr18'
    for lang in LANGS_SR18:
        nlp_tokenise = stanfordnlp.Pipeline(lang=lang, processors='tokenize,mwt')
        nlp = stanfordnlp.Pipeline(tokenize_pretokenized=True, lang=lang, processors='tokenize,mwt,pos,lemma,depparse')
        for team in TEAMS_SR18:
            try:
                sentences = output_templates_to_list(nlp, nlp_tokenise,
                SUBMISSIONS_SR18 + team + '/T1/' + lang + '_out.txt')
                print(team, len(sentences))
                filename = f'{team}_{lang}_out.conllu'
                with open(path + '/' + filename, 'w+') as f:
                    f.write(''.join(sentences))
            except FileNotFoundError:
                pass


def parse_sr19():
    path = 'preprocessed_outputs/sr19'
    for lang_long in LANGS_SR19:
        lang = lang_long.split('_')[0]
        # use default models for parsing
        nlp_tokenise = stanfordnlp.Pipeline(lang=lang, processors='tokenize,mwt')
        nlp = stanfordnlp.Pipeline(tokenize_pretokenized=True, lang=lang, processors='tokenize,mwt,pos,lemma,depparse')
        for team in TEAMS_SR19:
            filename = SUBMISSIONS_SR19 + team + '/T1/' + lang_long + '-ud-test.txt'
            if os.path.isfile(filename):
                print('Started parsing', filename)
                sentences = output_templates_to_list(nlp, nlp_tokenise, filename)
                print(team, len(sentences))
                # filename = f'{team}_{lang}_out.txt'
                filename_out = f'{team}_{lang_long}_out.conllu'
                with open(path + '/' + filename_out, 'w+') as f:
                    f.write(''.join(sentences))


def parse_args():
    parser = argparse.ArgumentParser(description='Parse system outputs with stanfordnlp.')
    parser.add_argument('--download_models', dest='modeldownload', action='store_true',
                        help='Download pretrained models provided by stanfordnlp.')
    parser.set_defaults(modeldownload=False)
    args_init = parser.parse_args()
    return args_init


if __name__ == '__main__':
    args = parse_args()
    if args.modeldownload:
        download_models()
    parse_sr18()
    parse_sr19()

