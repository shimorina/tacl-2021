import conllu
from collections import Counter
import pandas as pd


def read_conllu(filepath):
    sentences = []
    data_file = open(filepath, "r", encoding="utf-8")
    # except conllu.parser.ParseException:
    # sentences_trees = []  # todo: empty realisations
    for tokenlist in conllu.parse_tree_incr(data_file):
        sentences.append(tokenlist)
    return sentences


def read_conllu_linear(filepath):
    sentences = []
    data_file = open(filepath, "r", encoding="utf-8")
    for tokenlist in conllu.parse_incr(data_file):
        sentences.append(tokenlist)
    return sentences


# todo: empty sentence
def produce_tuples(sentences_linear):
    triples = []
    for sentence_linear in sentences_linear:
        sentence_tuples = []
        for token in sentence_linear:
            head = token['head']
            if head != 0:
                head_lemma = sentence_linear[head - 1]['lemma']
                pair = (token['lemma'], head_lemma, token['deprel'])
            else:
                pair = (token['lemma'], head, token['deprel'])
            sentence_tuples.append(pair)
        triples.append(sentence_tuples)
    return triples


def tuples_to_string(tuples, score):
    # list of tuples
    if score == 'uas':
        tuples_string = [f'{tup[0]}-{tup[1]}'.lower() for tup in tuples]  # lemma-head_lemma
    elif score == 'las':
        tuples_string = [f'{tup[0]}-{tup[1]}-{tup[2]}'.lower() for tup in tuples]  # lemma-head_lemma-deprel
    else:
        print('Only UAS and LAS are supported.')
    return tuples_string


def calculate_score(tuples_gold, tuples_system, metric):
    tuples_gold_string = Counter(tuples_to_string(tuples_gold, metric))
    tuples_system_string = Counter(tuples_to_string(tuples_system, metric))
    # common elements of two lists including repetitions
    intersection = tuples_gold_string & tuples_system_string
    # (U/L)AS for a sentence
    # metric_s = len(list(intersection.elements())) / len(tuples_gold_string)
    # return number of correct relations
    return len(list(intersection.elements()))


def calculate_uas(goldfile, systemfile):
    sentences_gold = produce_tuples(read_conllu_linear(goldfile))
    sentences_system = produce_tuples(read_conllu_linear(systemfile))
    assert len(sentences_gold) == len(sentences_system), 'Files are not of the same length'
    scores_uas = []  # sentence-based scores (macro-average)
    scores_las = []  # sentence-based scores (macro-average)
    correct_relations_total_uas = 0
    correct_relations_total_las = 0
    system_nodes = 0
    gold_nodes = 0
    for tuples_gold, tuples_system in zip(sentences_gold, sentences_system):
        correct_uas = calculate_score(tuples_gold, tuples_system, 'uas')
        correct_las = calculate_score(tuples_gold, tuples_system, 'las')
        uas_s = correct_uas / len(tuples_gold)
        las_s = correct_las / len(tuples_gold)
        scores_uas.append(uas_s)
        scores_las.append(las_s)
        # micro-average
        correct_relations_total_uas += correct_uas
        correct_relations_total_las += correct_las
        system_nodes += len(tuples_system)
        gold_nodes += len(tuples_gold)
    precision_uas = correct_relations_total_uas / system_nodes
    recall_uas = correct_relations_total_uas / gold_nodes
    precision_las = correct_relations_total_las / system_nodes
    recall_las = correct_relations_total_las / gold_nodes
    # F1 = 2PR/P+R
    f1_uas = (2 * precision_uas * recall_uas) / (precision_uas + recall_uas)
    f1_las = (2 * precision_las * recall_las) / (precision_las + recall_las)
    print('F1 (UAS):', round(f1_uas * 100, 2))
    print('F1 (LAS):', round(f1_las * 100, 2))
    # print('macro-average UAS:', round((sum(scores_uas)/len(scores_uas)) * 100, 2))
    # print('macro-average LAS:', round((sum(scores_las)/len(scores_las)) * 100, 2))
    # print('micro-average UAS:', round(precision_uas * 100, 2))
    # print('micro-average LAS:', round(precision_las * 100, 2))
    print('')
    return round(f1_uas * 100, 2), round(f1_las * 100, 2)

